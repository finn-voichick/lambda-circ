(* LambdaCirc - a simple functional reversible circuit construction language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of LambdaCirc.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import String.

From LambdaCirc Require Import LambdaCirc.

Inductive expr : Set :=
  | expr_unit
  | expr_inl (t : expr)
  | expr_inr (t : expr)
  | expr_pair (t1 t2 : expr)
  | expr_var (x : string)
  | expr_let (x1 x2 : string) (t' t : expr)
  | expr_case
      (t : expr) (x0 : string) (t0 : expr) (x1 : string) (t1 : expr)
  | expr_if (t t0 t1 : expr).

Inductive data_ty : Set :=
  | data_unit
  | data_sum (T0 T1 : data_ty)
  | data_prod (T1 T2 : data_ty).

Declare Custom Entry data_ty.

Notation "<[[ T ]]>" := T (T custom data_ty at level 99).
Notation "( x )" := x (in custom data_ty, x at level 99).
Notation "x" := x (in custom data_ty at level 0, x constr at level 0).
Notation "'Unit'" := data_unit (in custom data_ty).
Notation "T0 + T1" :=
  (data_sum T0 T1)
  (in custom data_ty at level 3,
   left associativity).
Notation "T1 * T2" :=
  (data_prod T1 T2)
  (in custom data_ty at level 2,
   T1 custom data_ty,
   T2 custom data_ty at level 0).
Notation "'Bit'" := <{{ Unit + Unit }}> (in custom data_ty). 

Fixpoint ty_size (T : data_ty) :=
  match T with
  | <[[ Unit ]]> => 0
  | <[[ T0 + T1 ]]> => 1 + max (ty_size T0) (ty_size T1)
  | <[[ T1 * T2 ]]> => ty_size T1 + ty_size T2
  end.

Inductive rcir : Set :=
  | rcir_skip
  | rcir_not (q : nat)
  | rcir_ctrl (q : nat) (p : rcir)
  | rcir_seq (p1 p2 : rcir).
