(* LambdaCirc - a simple functional reversible circuit construction language
 * Copyright (C) 2021 University of Maryland
 *
 * This file is part of LambdaCirc.
 *
 * LambdaCirc is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses>.
 *)

Require Import Bool.
Require Import String.
Require Import List.

From PLF Require Import Maps.

Inductive tm : Set :=
  | tm_unit
  | tm_inl (t : tm)
  | tm_inr (t : tm)
  | tm_pair (t1 t2 : tm)
  | tm_var (x : string)
  | tm_case (t : tm) (x0 : string) (t0 : tm) (x1 : string) (t1 : tm)
  | tm_if (t t0 t1 : tm)
  | tm_let (x1 x2 : string) (t' t : tm)
  | tm_abs (x : string) (t : tm)
  | tm_app (t' t : tm).

Declare Custom Entry lambda_circ.

Notation "<{ t }>" := t (t custom lambda_circ at level 99).
Notation "x" := x (in custom lambda_circ at level 0, x constr at level 0).
Notation "( x )" := x (in custom lambda_circ, x at level 99).
Notation "<>" := tm_unit (in custom lambda_circ).
Notation "'inl' t" := (tm_inl t) (in custom lambda_circ at level 0).
Notation "'inr' t" := (tm_inr t) (in custom lambda_circ at level 0).
Notation "< t1 , t2 >" := (tm_pair t1 t2) (in custom lambda_circ).
Coercion tm_var : string >-> tm.
Notation "'case' t 'of' | 'inl' x0 => t0 | 'inr' x1 => t1" :=
  (tm_case t x0 t0 x1 t1)
  (in custom lambda_circ at level 89,
   t custom lambda_circ at level 99,
   x0 custom lambda_circ at level 99,
   t0 custom lambda_circ at level 99,
   x1 custom lambda_circ at level 99,
   t1 custom lambda_circ at level 99,
   left associativity).
Notation "'case' t 'of' | 0 => t0 | 1 => t1" :=
  (tm_if t t0 t1)
  (in custom lambda_circ at level 89,
   t custom lambda_circ at level 99,
   t0 custom lambda_circ at level 99,
   t1 custom lambda_circ at level 99,
   left associativity).
Notation "'let' < x , y > = t' 'in' t" :=
  (tm_let x y t' t) (in custom lambda_circ at level 0).
Notation "\ x , t" :=
  (tm_abs x t)
  (in custom lambda_circ at level 90,
   x at level 99,
   t custom lambda_circ at level 99,
   left associativity).
Notation "t' t" :=
  (tm_app t' t) (in custom lambda_circ at level 1, left associativity).
Notation "{ x }" := x (in custom lambda_circ at level 1, x constr).

Notation "0" := <{ inl <> }> (in custom lambda_circ).
Notation "1" := <{ inr <> }> (in custom lambda_circ).

Definition c : string := "c".
Definition s : string := "s".
Definition x : string := "x".
Definition x' : string := "x'".
Definition x0 : string := "x0".
Definition y : string := "y".
Definition y' : string := "y'".
Definition y0 : string := "y0".

Reserved Notation "[ x := s ] t" (in custom lambda_circ at level 20, x constr).
Fixpoint subst (x : string) (s : tm) (t : tm) : tm :=
  match t with
  | <{ <> }> => <{ <> }>
  | <{ inl t' }> => <{ inl ([x:=s] t') }>
  | <{ inr t' }> => <{ inr ([x:=s] t') }>
  | <{ <t1, t2> }> => <{ <[x:=s] t1, [x:=s] t2> }>
  | tm_var y => if eqb_string x y then s else t
  | <{ let <x1, x2> = t1 in t2 }> =>
      let t2' := if eqb_string x x1 || eqb_string x x2
                 then t2
                 else <{ [x:=s] t2 }> in
      <{ let <x1, x2> = [x:=s] t1 in t2' }>
  | <{ case t' of | inl x0 => t0 | inr x1 => t1 }> =>
      let t0' := if eqb_string x x0 then t0 else <{ [x:=s] t0 }> in
      let t1' := if eqb_string x x1 then t1 else <{ [x:=s] t1 }> in
      <{ case [x:=s] t' of | inl x0 => t0' | inr x1 => t1' }>
  | <{ case t' of | 0 => t0 | 1 => t1 }> =>
      <{ case [x:=s] t' of | 0 => [x:=s] t0 | 1 => [x:=s] t1 }>
  | <{ \y, t' }> => if eqb_string x y then t else <{ \y, [x:=s] t' }>
  | <{ t1 t2 }> => <{ ([x:=s] t1) ([x:=s] t2) }>
  end
where "[ x := s ] t" := (subst x s t) (in custom lambda_circ).

Inductive value : tm -> Prop :=
  | value_unit : value <{ <> }>
  | value_inl v : value v -> value <{ inl v }>
  | value_inr v : value v -> value <{ inr v }>
  | value_pair v1 v2 : value v1 -> value v2 -> value <{ <v1, v2> }>
  | value_abs x t : value <{ \x, t }>.

Reserved Notation "t --> t'" (at level 40).
Inductive step : tm -> tm -> Prop :=
  | step_app_abs x t v : 
      value v ->
      <{ (\x, t) v }> --> <{ [x:=v] t }>
  | step_app1 t1 t1' t2 :
      t1 --> t1' ->
      <{ t1 t2 }> --> <{ t1' t2 }>
  | step_app2 v t t' :
      value v ->
      t --> t' ->
      <{ v t }> --> <{ v t' }>
  | step_inl t t' :
      t --> t' ->
      <{ inl t }> --> <{ inl t' }>
  | step_inr t t' :
      t --> t' ->
      <{ inr t }> --> <{ inr t' }>
  | step_pair1 t1 t1' t2 :
      t1 --> t1' ->
      <{ <t1, t2> }> --> <{ <t1', t2> }>
  | step_pair2 v t t' :
      value v ->
      t --> t' ->
      <{ <v, t> }> --> <{ <v, t'> }>
  | step_let x1 x2 t1 t1' t2 :
      t1 --> t1' ->
      <{ let <x1, x2> = t1 in t2 }> --> <{ let <x1, x2> = t1' in t2 }>
  | step_let_pair x1 x2 v1 v2 t :
      value v1 ->
      value v2 ->
      <{ let <x1, x2> = <v1, v2> in t }> --> <{ [x1:=v1] [x2:=v2] t }>
  | step_case t t' x0 t0 x1 t1 :
      t --> t' ->
      <{ case t of | inl x0 => t0 | inr x1 => t1 }> -->
      <{ case t' of | inl x0 => t0 | inr x1 => t1 }>
  | step_case_inl v x0 t0 x1 t1 :
      value v ->
      <{ case inl v of | inl x0 => t0 | inr x1 => t1 }> --> <{ [x0:=v] t0 }>
  | step_case_inr v x0 t0 x1 t1 :
      value v ->
      <{ case inr v of | inl x0 => t0 | inr x1 => t1 }> --> <{ [x1:=v] t1 }>
  | step_if t t' t0 t1 :
      t --> t' ->
      <{ case t of | 0 => t0 | 1 => t1 }> -->
      <{ case t' of | 0 => t0 | 1 => t1 }>
  | step_if_0 t0 t1 :
      <{ case 0 of | 0 => t0 | 1 => t1 }> --> <{ t0 }>
  | step_if_1 t0 t1 :
      <{ case 1 of | 0 => t0 | 1 => t1 }> --> <{ t1 }>
where "t --> t'" := (step t t').

Inductive ty : Set :=
  | ty_unit
  | ty_sum (T1 T2 : ty)
  | ty_prod (T1 T2 : ty)
  | ty_fun (T T' : ty).

Declare Custom Entry lambda_circ_ty.

Notation "<{{ T }}>" := T (T custom lambda_circ_ty at level 99).
Notation "( x )" := x (in custom lambda_circ_ty, x at level 99).
Notation "x" := x (in custom lambda_circ_ty at level 0, x constr at level 0).
Notation "'Unit'" := ty_unit (in custom lambda_circ_ty).
Notation "T0 + T1" :=
  (ty_sum T0 T1)
  (in custom lambda_circ_ty at level 3,
   left associativity).
Notation "T1 * T2" :=
  (ty_prod T1 T2)
  (in custom lambda_circ_ty at level 2,
   T1 custom lambda_circ_ty,
   T2 custom lambda_circ_ty at level 0).
Notation "T1 -> T2" :=
  (ty_fun T1 T2)
  (in custom lambda_circ_ty at level 50,
   right associativity).
Notation "'Bit'" := <{{ Unit + Unit }}> (in custom lambda_circ_ty). 

(* an n-bit register *)
Reserved Notation "Reg[ n ]" (in custom lambda_circ_ty).
Fixpoint ty_reg n := match n with
                     | 0 => <{{ Unit }}>
                     | S n' => <{{ Bit * Reg[n'] }}>
                     end
where "Reg[ n ]" := (ty_reg n) (in custom lambda_circ_ty).

(* an n-bit integer, essentially a list of Bits *)
Reserved Notation "Int[ n ]" (in custom lambda_circ_ty).
Fixpoint ty_int n := match n with
                     | 0 => <{{ Unit }}>
                     | S n' => <{{ Int[n'] * Bit }}>
                     end
where "Int[ n ]" := (ty_int n) (in custom lambda_circ_ty).

Definition context := partial_map ty.

Reserved Notation "Gamma |- t : T"
    (at level 40, t custom lambda_circ, T custom lambda_circ_ty at level 0).
Inductive has_type : context -> tm -> ty -> Prop :=
  | has_type_unit Gamma :
      Gamma |- <> : Unit
  | has_type_inl Gamma t T0 T1 :
      Gamma |- t : T0 ->
      Gamma |- inl t : (T0 + T1)
  | has_type_inr Gamma t T0 T1 :
      Gamma |- t : T1 ->
      Gamma |- inr t : (T0 + T1)
  | has_type_pair Gamma t1 t2 T1 T2 :
      Gamma |- t1 : T1 ->
      Gamma |- t2 : T2 ->
      Gamma |- <t1, t2> : (T1 * T2)
  | has_type_var Gamma x T :
      Gamma x = Some T ->
      Gamma |- x : T
  | has_type_let Gamma x1 x2 t' t T1 T2 T :
      x1 <> x2 ->
      Gamma |- t' : (T1 * T2) ->
      (x1 |-> T1; x2 |-> T2; Gamma) |- t : T ->
      Gamma |- let <x1, x2> = t' in t : T
  | has_type_case Gamma t x0 t0 x1 t1 T0 T1 T :
      Gamma |- t : (T0 + T1) ->
      (x0 |-> T0; Gamma) |- t0 : T ->
      (x1 |-> T1; Gamma) |- t1 : T ->
      Gamma |- case t of | inl x0 => t0 | inr x1 => t1 : T
  | has_type_if Gamma t t0 t1 T :
      Gamma |- t : Bit ->
      Gamma |- t0 : T ->
      Gamma |- t1 : T ->
      Gamma |- case t of | 0 => t0 | 1 => t1 : T
  | has_type_abs Gamma x t T T' :
      (x |-> T; Gamma) |- t : T' ->
      Gamma |- \x, t : (T -> T')
  | has_type_app Gamma f t T T' :
      Gamma |- f : (T' -> T) ->
      Gamma |- t : T' ->
      Gamma |- f t : T
where "Gamma |- t : T" := (has_type Gamma t T).

Notation "|- t : T" :=
    (empty |- t : T)
    (at level 40, t custom lambda_circ, T custom lambda_circ_ty at level 0).

Lemma has_type_0 :
  forall Gamma,
  Gamma |- 0 : Bit.
Proof. eauto using has_type. Qed.

Lemma has_type_1 :
  forall Gamma,
  Gamma |- 1 : Bit.
Proof. eauto using has_type. Qed.

Example identity := <{ \x,x }>.
Definition constant (t : tm) := <{ \x,t }>.

Fixpoint succ (n : nat) := match n with
                           | 0 => constant <{ <> }>
                           | S n' => 
                               <{ \x,
                                 let <x', x0> = x in
                                 case x0 of
                                 | 0 => <x', 1>
                                 | 1 => < {succ n'} x', 0> }>
                           end.

(* Add three bits *)
Definition adder :=
  <{ \x, \y, \c,
     case x of
     | 0 => case y of
            | 0 => <0, c>
            | 1 => case c of
                   | 0 => <0, 1>
                   | 1 => <1, 0>
     | 1 => case y of
            | 0 => case c of
                   | 0 => <0, 1>
                   | 1 => <1, 0>
            | 1 => <1, c> }>.

Lemma has_type_adder :
  forall Gamma,
  Gamma |- adder : (Bit -> Bit -> Bit -> Bit * Bit).
Proof. eauto 50 using has_type, has_type_0, has_type_1. Qed.

(* Add two n-bit integers, with a carry bit *)
Fixpoint add_carry n :=
  match n with
  | 0 => <{ \c, \x, \y, <> }> (* base case: 0 is the only 0-bit integer *)
  | S n' => <{ \c, \x, \y,
               let <x', x0> = x in
               let <y', y0> = y in
               let <c, s> = adder x0 y0 c in (* name shadowing *)
               < { add_carry n' } c x' y', s> }>
  end.

Lemma has_type_add_carry :
  forall n Gamma,
  Gamma |- {add_carry n} : (Bit -> Int[n] -> Int[n] -> Int[n]).
Proof.
  intros n. induction n as [| n']; eauto using has_type.
  intros Gamma. simpl. repeat constructor. econstructor.
  - discriminate.
  - constructor. reflexivity.
  - econstructor.
    + discriminate.
    + constructor. reflexivity.
    + econstructor.
      * discriminate.
      * econstructor.
        -- econstructor.
           ++ econstructor; auto using has_type_adder.
              constructor. reflexivity.
           ++ constructor. reflexivity.
        -- constructor. reflexivity.
      * constructor.
        -- econstructor.
           ++ econstructor.
              ** eauto using has_type.
              ** constructor. reflexivity.
           ++ constructor. reflexivity.
        -- constructor. reflexivity.
Qed.

(* Add two n-bit integers *)
Definition add n := <{ { add_carry n } 0 }>.

Lemma has_type_add :
  forall n,
  |- { add n } : (Int[n] -> Int[n] -> Int[n]).
Proof.
  intros n. unfold add. eauto using has_type, has_type_add_carry, has_type_0.
Qed.
